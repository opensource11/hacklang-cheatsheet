# Language

* Case sensitive

## Comments
* Single line:
´´´
#
//
´´´
* Multiline
´´´
/*
*/
´´´
* Doc
´´´
/**
*/
´´´
* Special comments:
´´´
// FALLTHROUGH
// strict and // partial
/* HH_FIXME[1234] */ or /* HH_IGNORE_ERROR[1234] */
´´´
## Identifiers

* Must start with either: 
  * Upper-case letter
  * Lower-case letter
  * Underscore _
* Variables must be preceded with $?

## Literals

* Boolean: 
  * true
  * false
* Integers:
  * Decimal
  * Hexadecimal 
  * Octal
  * Binary
* Floating-point
  * Supportrt for Exponencial notation: 1e2 or 1E2
  * INF constant for infinity
  * NAN constant for not-a-number
* Null
  * null
* Strings
  * Single quote:
    * 'Hello world'
    * Single quote and backslash are represented using escape sequences \' and \\.
  * Double quote
    * "Hello world"
    * Double quote and backslash are represented using escape sequences \" and \\.
    * Words starting with $ are sustituted by variable values:
      * To prevent sutistution escape $ with a backlash.
      * The ustitution converts to string the value.
      * If no variable is found to match the identifier then there is not sustitution.

  * Heredoc
    * Supports variable sustitution
    <<< id 
    id
  * Nowdoc
    * Doesn't support variable sustitution
<<<    'ID'
 id
  * Escape sequences for double quote and Heredoc:

| ESCAPE SEQUENCE	| CHARACTER NAME                               | UNICODE CHARACTER |
|-----------------|----------------------------------------------|-------------------|
| $	              | Dollar sign	                                 | U+0024            |
| "	              | Double quote	                               | U+0022            |
| \\              | Backslash	                                   | U+005C            |
| \e	            | Escape	                                     | U+001B            |
| \f	            | Form feed	                                   | U+000C            |
| \n	            | New line	                                   | U+000A            |
| \r	            | Carriage Return	                             | U+000D            |
| \t	            | Horizontal Tab	                             | U+0009            |
| \v	            | Vertical Tab	                               | U+000B            |
| \ooo	          | 1-3-digit octal digit value 	               | ooo               |
| \xhh or \Xhh	  | 1-2-digit hexadecimal digit value hh         | U+00hh            |
| \u{xxxxxx}	    | UTF-8 encoding of Unicode codepoint U+xxxxxx | U+xxxxxx          |


## Reserved Words
abstract
arraykey
as
async
await
break
case
catch
class
classname
clone
const
continue
default
do
dynamic
echo
else
elseif
enum
extends
final
finally
for
foreach
function
if
implements
inout
instanceof
insteadof
interface
mixed
namespace
new
newtype
noreturn
num
parent
private
protected
public
require
require_once
return
self
shape
static
switch
throw
trait
try
tuple
type
use
while
yield


